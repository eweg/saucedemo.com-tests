package saucedemoTests;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.HomePage;
import saucedemoTests.utility.TestUtility;

public class CartTest {

    private HomePage homePage;
    private CartPage cartPage;

    @BeforeMethod
    private void prepareTest() {

        homePage = TestUtility.openWebsite();

    }

    @AfterMethod
    private void closeWebsite() {

        Selenide.clearBrowserCookies();
        Selenide.closeWebDriver();

    }

    @Test
    public void addTheProductToCartThenCorrectProductAndCorrectQuantityAreInCart() {

        homePage.addBackpackToCart();
        cartPage = homePage.goToCart();
        cartPage.assertSauceLabsBackpackIsInCart();
        cartPage.assertCorrectSauceLabsBackpackQuantity();

    }

    @Test
    public void addAllTypeOfProductsToCartThenQuantitiesAreCorrect() {

        int quantityOfAddedProductsToCart = homePage.addAllProductsToCart();
        cartPage = homePage.goToCart();
        cartPage.assertQuantityAfterAddAllProductsToCart(quantityOfAddedProductsToCart);

    }

    @Test
    public void useContinueShoppingThenGoToHomePageAndHaveChosenProductInCart() {

        homePage.addBackpackToCart();
        cartPage = homePage.goToCart();
        cartPage.clickContinueShoppingButton();
        homePage.assertAccessedHomePage();
        homePage.assertAddBackpackToCart();

    }

    @Test
    public void useRemoveButtonThenProductIsRemovedFromList() {

        homePage.addBackpackToCart();
        cartPage = homePage.goToCart();
        cartPage.clickRemoveBackpackFromCart();
        cartPage.assertWorkRemoveButton();

    }
}
