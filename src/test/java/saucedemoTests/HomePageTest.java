package saucedemoTests;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.*;
import pages.HomePage;
import saucedemoTests.utility.TestUtility;

public class HomePageTest {

    private HomePage homePage;

    @BeforeMethod
    private void prepareTest() {

        homePage = TestUtility.openWebsite();

    }

    @AfterMethod
    private void closeWebsite() {

        Selenide.clearBrowserCookies();
        Selenide.closeWebDriver();

    }

    @Test
    public void sortProductsByPriceLowToHighThenFirstIsTheCheapestOne() {

        homePage.sortProductsBy("lohi");
        homePage.assertSortProductsByPrice("$7.99");

    }

    @Test
    public void sortProductsByPriceHighToLowThenFirstIsTheMostExpensiveOne() {

        homePage.sortProductsBy("hilo");
        homePage.assertSortProductsByPrice("$49.99");

    }

    @Test
    public void addTheProductToCartThenOneProductIsOnCartLabelAndThisProductHasRemoveButton() {

        homePage.addBackpackToCart();
        homePage.assertAddBackpackToCart();

    }

    @Test
    public void addAllProductsToCartThenProperQuantityIsOnCartLabelAndEveryProductHasRemoveButton() {

        homePage.addAllProductsToCart();
        homePage.assertAddAllProductsToCart();

    }
}
