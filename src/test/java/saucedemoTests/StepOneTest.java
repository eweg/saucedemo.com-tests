package saucedemoTests;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.*;
import saucedemoTests.utility.TestUtility;

public class StepOneTest {

    private StepOnePage stepOnePage;

    @BeforeMethod
    private void prepareTest() {

        HomePage homePage = TestUtility.openWebsite();
        homePage.addBackpackToCart();
        CartPage cartPage = homePage.goToCart();
        stepOnePage = cartPage.clickCheckoutButton();

    }

    @AfterMethod
    private void closeWebsite() {

        Selenide.clearBrowserCookies();
        Selenide.closeWebDriver();

    }

    @Test
    public void fillFormCorrectlyThenCanGoToNextStep() {

        stepOnePage.fillForm("tester", "11-111");
        StepTwoPage stepTwoPage = stepOnePage.clickContinueButton();
        stepTwoPage.assertAccessedCheckoutStepTwoPage();

    }

    @Test
    public void clickContinueWithEmptyFormThenShowCorrectErrorMessage() {

        stepOnePage.clickContinueButton();
        stepOnePage.assertShowCorrectErrorMessage("Error: First Name is required");

    }

    @Test
    public void clickContinueWithoutNamesThenShowCorrectErrorMessage() {

        stepOnePage.fillForm("", "11-111");
        stepOnePage.clickContinueButton();
        stepOnePage.assertShowCorrectErrorMessage("Error: First Name is required");

    }

    @Test
    public void clickContinueWithoutPostalCodeThenShowCorrectErrorMessage() {

        stepOnePage.fillForm("tester", "");
        stepOnePage.clickContinueButton();
        stepOnePage.assertShowCorrectErrorMessage("Error: Postal Code is required");

    }

    @Test
    public void clickCancelButtonThenBackToCartPageAndCorrectProductInCard(){

        CartPage cartPage = stepOnePage.clickCancelButton();
        cartPage.assertSauceLabsBackpackIsInCart();
        cartPage.assertCorrectSauceLabsBackpackQuantity();

    }
}
