package saucedemoTests;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import saucedemoTests.utility.TestUtility;
import utility.MainMenu;

import static com.codeborne.selenide.Selenide.open;

public class MainMenuTest {

    private HomePage homePage;
    private MainMenu mainMenu = new MainMenu();

    @BeforeMethod
    private void prepareTest() {

        homePage = TestUtility.openWebsite();

    }

    @AfterMethod
    private void closeWebsite() {

        Selenide.clearBrowserCookies();
        Selenide.closeWebDriver();

    }


    @Test
    public void useResetAppStateThenCartLabelWithQuantityOfProductsIsntExist() {

        homePage.addBackpackToCart();
        homePage.assertAddBackpackToCart();
        mainMenu.useResetAppState();
        homePage.assertUseResetAppState();

    }

    @Test
    public void linksRedirectToCorrectUrl() {

        mainMenu.rollOutMenu();
        mainMenu.assertAccessedFullMenu();
        open("https://saucelabs.com/");
        mainMenu.assertCorrectLinksInFullMenu();

    }
}
