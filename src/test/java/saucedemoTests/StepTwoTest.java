package saucedemoTests;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.*;
import saucedemoTests.utility.TestUtility;

public class StepTwoTest {

    private StepTwoPage stepTwoPage;

    @BeforeMethod
    public void prepareTest() {

        HomePage homePage = TestUtility.openWebsite();
        homePage.addBackpackToCart();
        CartPage cartPage = homePage.goToCart();
        StepOnePage stepOnePage = cartPage.clickCheckoutButton();
        stepOnePage.fillForm("tester", "11-111");
        stepTwoPage = stepOnePage.clickContinueButton();

    }

    @AfterMethod
    private void closeWebsite() {

        Selenide.clearBrowserCookies();
        Selenide.closeWebDriver();

    }

    @Test
    public void clickFinishButtonThenCanGoToCheckoutCompletePage() {

        stepTwoPage.assertAccessedCheckoutStepTwoPage();
        CheckoutCompletePage checkoutCompletePage = stepTwoPage.clickFinishButton();
        checkoutCompletePage.assertAccessedCheckoutCompletePage();

    }

    @Test
    public void listShowsCorrectDataAboutChosenProduct() {

        stepTwoPage.assertCorrectProductAndQuantityAndPriceOnList();

    }
}
