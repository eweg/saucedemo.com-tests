package saucedemoTests;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import settings.Settings;

import static com.codeborne.selenide.Selenide.open;

public class LoginTest {

    private LoginPage loginPage;

    @BeforeSuite
    public static void startTests() {

        Settings.getProperties();

    }

    @BeforeMethod
    private void openWebsite() {

        loginPage = open("https://www.saucedemo.com", LoginPage.class);

    }

    @AfterTest
    private void closeWebsite() {

        Selenide.clearBrowserCookies();
        Selenide.closeWebDriver();

    }

    @Test
    public void logInSuccessfullyByStandardUserThenGoToHomepage() {

        HomePage homepage = loginPage.logInSuccessfully("standard_user", "secret_sauce");
        homepage.assertAccessedHomePage();

    }

    @Test
    public void logInWithIncorrectUsernameThenShowErrorMessage() {

        loginPage.logIn("incorrect_username", "secret_sauce");
        loginPage.assertFailedLogIn("Epic sadface: Username and password do not match any user in this service");

    }

    @Test
    public void logInWithIncorrectPasswordThenShowErrorMessage() {

        loginPage.logIn("standard_user", "incorrect_password");
        loginPage.assertFailedLogIn("Epic sadface: Username and password do not match any user in this service");

    }

    @Test
    public void logInByLockedOutUserThenShowErrorMessage() {

        loginPage.logIn("locked_out_user", "secret_sauce");
        loginPage.assertFailedLogIn("Epic sadface: Sorry, this user has been locked out.");

    }

    @Test
    public void logInByPerformanceGlitchUser() {

        HomePage homePage = loginPage.logInSuccessfully("performance_glitch_user", "secret_sauce");
        homePage.assertAccessedHomePage();

    }
}