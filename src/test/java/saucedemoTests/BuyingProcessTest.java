package saucedemoTests;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.*;
import saucedemoTests.utility.TestUtility;

public class BuyingProcessTest {

    private HomePage homePage;

    @BeforeMethod
    private void prepareTest() {

        homePage = TestUtility.openWebsite();

    }

    @AfterMethod
    private void closeWebsite() {

        Selenide.clearBrowserCookies();
        Selenide.closeWebDriver();

    }

    @Test
    public void buyProductCorrectly() {

        CartPage cartPage;

        homePage.addBackpackToCart();
        homePage.assertAddBackpackToCart();
        cartPage = homePage.goToCart();

        cartPage.assertSauceLabsBackpackIsInCart();
        cartPage.assertCorrectSauceLabsBackpackQuantity();
        StepOnePage stepOnePage = cartPage.clickCheckoutButton();

        stepOnePage.assertAccessedCheckoutStepOnePage();
        stepOnePage.fillForm("tester", "11-111");
        StepTwoPage stepTwoPage = stepOnePage.clickContinueButton();

        stepTwoPage.assertAccessedCheckoutStepTwoPage();
        CheckoutCompletePage checkoutCompletePage = stepTwoPage.clickFinishButton();

        checkoutCompletePage.assertAccessedCheckoutCompletePage();

    }
}
