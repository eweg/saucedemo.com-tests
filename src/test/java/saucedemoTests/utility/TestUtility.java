package saucedemoTests.utility;

import pages.HomePage;
import pages.LoginPage;

import static com.codeborne.selenide.Selenide.open;

public class TestUtility {

    public static HomePage openWebsite() {

        LoginPage loginPage = open("https://www.saucedemo.com", LoginPage.class);
        return loginPage.logInSuccessfully("standard_user", "secret_sauce");

    }
}
