package settings;

import com.codeborne.selenide.Configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Settings {

    private static Properties properties;

    private static void loadProperties() {
        properties = new Properties();
        try {

            InputStream propertiesHandler = new FileInputStream("src/main/resources/config.properties");
            properties.load(propertiesHandler);

            Configuration.headless = Boolean.parseBoolean(getProperty("headlessMode")); // it works only for Chrome
            Configuration.browser = getProperty("browser"); // command to run: mvn test -Dselenide.browser=firefox
            Configuration.timeout = Long.parseLong(getProperty("defaultTimeout"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Properties getProperties() {

        if (properties == null) {
            loadProperties();

        }
        return properties;
    }

    public static String getProperty(String key) {

        return getProperties().getProperty(key);

    }
}