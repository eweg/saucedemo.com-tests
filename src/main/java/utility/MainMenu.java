package utility;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import pages.LoginPage;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverConditions.url;

public class MainMenu {

    private final SelenideElement MENU_BUTTON = $("#react-burger-menu-btn");
    private final SelenideElement FULL_MENU_DIV = $(".bm-menu-wrap");
    private final ElementsCollection LINKS_IN_MENU_A = $$(".menu-item");
    private final SelenideElement RESET_APP_STATE_IN_MENU_A = $("#reset_sidebar_link");
    private final SelenideElement QUANTITY_OF_PRODUCTS_ON_CART_LABEL_SPAN = $(".shopping_cart_badge");

    public void rollOutMenu() {

        MENU_BUTTON.click();

    }

    public void assertAccessedFullMenu() {

        FULL_MENU_DIV.shouldBe(Condition.visible, Condition.enabled);
        LINKS_IN_MENU_A.should(CollectionCondition.texts("All Items", "About", "Logout", "Reset App State"),
                CollectionCondition.size(4));

    }

    public void useResetAppState() {

        rollOutMenu();
        RESET_APP_STATE_IN_MENU_A.click();

    }

    public void assertUseResetAppState() {

        QUANTITY_OF_PRODUCTS_ON_CART_LABEL_SPAN.shouldBe(Condition.not(Condition.exist));

    }

    public void assertCorrectLinksInFullMenu() {

        for (SelenideElement menuElement : LINKS_IN_MENU_A) {

            switch (menuElement.getAttribute("id")) {
                case "inventory_sidebar_link":
                case "reset_sidebar_link":
                    handleRedirection(menuElement, "https://www.saucedemo.com/inventory.html");
                    break;
                case "about_sidebar_link":
                    handleRedirection(menuElement, "https://saucelabs.com/");
                    break;
                case "logout_sidebar_link":
                    handleRedirection(menuElement, "https://www.saucedemo.com/");

                    LoginPage loginPage = new LoginPage();
                    loginPage.accessedLoginPage();
                    break;
            }
        }
    }

    private void handleRedirection(SelenideElement menuElement, String expectedURL) {

        menuElement.click();
        webdriver().shouldHave(url(expectedURL));
        open("https://www.saucedemo.com");
        rollOutMenu();

    }
}
