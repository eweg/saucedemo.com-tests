package pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class CartPage {

    private final SelenideElement SAUCE_LABS_BACKPACK_PRODUCT_NAME_A = $("#item_4_title_link");
    private final SelenideElement PRODUCT_QUANTITY_DIV = $(".cart_quantity");
    private final ElementsCollection PRODUCT_QUANTITY_DIVS = $$(".cart_quantity");
    private final SelenideElement CONTINUE_SHOPPING_BUTTON = $("#continue-shopping");
    private final SelenideElement REMOVE_BACKPACK_FROM_CART_BUTTON = $("#remove-sauce-labs-backpack");
    private final SelenideElement CHECKOUT_BUTTON = $("#checkout");


    public void assertSauceLabsBackpackIsInCart() {

        SAUCE_LABS_BACKPACK_PRODUCT_NAME_A.shouldHave(Condition.exactText("Sauce Labs Backpack"));

    }

    public void assertCorrectSauceLabsBackpackQuantity() {

        PRODUCT_QUANTITY_DIV.shouldHave(Condition.exactText("1"));

    }

    public void assertQuantityAfterAddAllProductsToCart(int quantityOfAddedProductsToCart) {

        PRODUCT_QUANTITY_DIVS.shouldHave(CollectionCondition.size(quantityOfAddedProductsToCart));

        for (SelenideElement singleProductQuantity : PRODUCT_QUANTITY_DIVS) {

            singleProductQuantity.should(Condition.exactText("1"));

        }
    }

    public void clickContinueShoppingButton() {

        CONTINUE_SHOPPING_BUTTON.click();

    }

    public void clickRemoveBackpackFromCart() {

        REMOVE_BACKPACK_FROM_CART_BUTTON.click();

    }

    public void assertWorkRemoveButton() {

        PRODUCT_QUANTITY_DIVS.shouldHave(CollectionCondition.size(0));

    }

    public StepOnePage clickCheckoutButton() {

        CHECKOUT_BUTTON.click();
        return page(StepOnePage.class);

    }
}
