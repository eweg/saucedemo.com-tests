package pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class HomePage {

    private final ElementsCollection PRODUCT_DIVS = $$(".inventory_item");
    private final SelenideElement SORT_BY_SELECT = $(".product_sort_container");
    private final ElementsCollection PRODUCT_PRICE_DIVS = $$(".inventory_item_price");
    private final SelenideElement ADD_TO_CART_SAUCE_LABS_BACKPACK_BUTTON = $("#add-to-cart-sauce-labs-backpack");
    private final SelenideElement QUANTITY_OF_PRODUCTS_ON_CART_LABEL_SPAN = $(".shopping_cart_badge");
    private final SelenideElement ADD_TO_CART_BUTTON = $(".btn_primary");
    private final SelenideElement REMOVE_FROM_CART_SAUCE_LABS_BACKPACK_BUTTON = $("#remove-sauce-labs-backpack");
    private final ElementsCollection REMOVE_BUTTONS = $$(".btn_secondary");
    private final SelenideElement CART_ICON_A = $(".shopping_cart_link");


    public void assertAccessedHomePage() {

        PRODUCT_DIVS.shouldHave(CollectionCondition.size(6));
        SORT_BY_SELECT.shouldBe(Condition.visible, Condition.enabled);

    }

    public void sortProductsBy(String option) {

        SORT_BY_SELECT.selectOptionByValue(option);

    }

    public void assertSortProductsByPrice(String price) {

        PRODUCT_PRICE_DIVS.first().shouldHave(Condition.exactText(price));

    }

    public void addBackpackToCart() {

        ADD_TO_CART_SAUCE_LABS_BACKPACK_BUTTON.click();

    }

    public CartPage goToCart() {

        CART_ICON_A.click();
        return page(CartPage.class);

    }

    public void assertAddBackpackToCart() {

        QUANTITY_OF_PRODUCTS_ON_CART_LABEL_SPAN.shouldHave(Condition.exactText("1"));
        REMOVE_FROM_CART_SAUCE_LABS_BACKPACK_BUTTON.shouldBe(Condition.visible, Condition.enabled);

    }

    public int addAllProductsToCart() {

        for (SelenideElement productDiv : PRODUCT_DIVS) {

            ADD_TO_CART_BUTTON.click();

        }
        return PRODUCT_DIVS.size();
    }

    public void assertAddAllProductsToCart() {

        QUANTITY_OF_PRODUCTS_ON_CART_LABEL_SPAN.shouldHave(Condition.exactText(String.valueOf(PRODUCT_DIVS.size())));
        REMOVE_BUTTONS.shouldHave(CollectionCondition.size(PRODUCT_DIVS.size()));

        for (SelenideElement removeButton : REMOVE_BUTTONS) {

            removeButton.shouldBe(Condition.visible, Condition.enabled);

        }
    }

    public void assertUseResetAppState() {

        QUANTITY_OF_PRODUCTS_ON_CART_LABEL_SPAN.shouldBe(Condition.not(Condition.exist));

    }
}