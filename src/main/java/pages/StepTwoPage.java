package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class StepTwoPage {

    private final SelenideElement HEADER_DIV = $("#header_container");
    private final SelenideElement FINISH_BUTTON = $("#finish");
    private final SelenideElement BACKPACK_ON_LIST_A = $("#item_4_title_link");
    private final SelenideElement PRODUCT_QUANTITY_DIV = $(".cart_quantity");
    private final SelenideElement PRODUCT_PRICE = $(".inventory_item_price");


    public void assertAccessedCheckoutStepTwoPage() {

        HEADER_DIV.lastChild().shouldHave(Condition.exactText("Checkout: Overview"));
        FINISH_BUTTON.shouldBe(Condition.visible, Condition.enabled);


    }

    public CheckoutCompletePage clickFinishButton() {

        FINISH_BUTTON.click();
        return page(CheckoutCompletePage.class);
    }

    public void assertCorrectProductAndQuantityAndPriceOnList() {

        BACKPACK_ON_LIST_A.shouldBe(Condition.visible, Condition.enabled);
        PRODUCT_QUANTITY_DIV.shouldHave(Condition.exactText("1"));
        PRODUCT_PRICE.shouldHave(Condition.exactText("$29.99"));

    }
}



