package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class StepOnePage {

    private final SelenideElement HEADER_DIV = $("#header_container");
    private final SelenideElement PERSONAL_DATA_DIV = $(".checkout_info");
    private final ElementsCollection FORM_INPUTS = $$(".form_input");
    private final SelenideElement CONTINUE_BTN_INPUT = $("#continue");
    private final SelenideElement ERROR_MESSAGE_DIV = $(".error-message-container");
    private final SelenideElement CANCEL_BUTTON = $("#cancel");


    public void assertAccessedCheckoutStepOnePage() {

        HEADER_DIV.lastChild().shouldHave(Condition.exactText("Checkout: Your Information"));
        PERSONAL_DATA_DIV.shouldBe(Condition.visible, Condition.enabled);
    }

    public void fillForm(String testName, String testCode) {

        for (SelenideElement formInput : FORM_INPUTS) {

            if (formInput.has(Condition.id("postal-code"))) {
                formInput.sendKeys(testCode);
            }
            else formInput.sendKeys(testName);
        }
    }

    public StepTwoPage clickContinueButton() {
        CONTINUE_BTN_INPUT.click();
        return page(StepTwoPage.class);

    }

    public void assertShowCorrectErrorMessage(String errorText) {

        ERROR_MESSAGE_DIV.shouldHave(Condition.exactText(errorText));

    }

    public CartPage clickCancelButton() {

        CANCEL_BUTTON.click();
        return page(CartPage.class);

    }
}
