package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class LoginPage {

    private final SelenideElement USERNAME_INPUT = $("#user-name");
    private final SelenideElement PASSWORD_INPUT = $("#password");
    private final SelenideElement LOGIN_BUTTON = $("#login-button");
    private final SelenideElement ERROR_MESSAGE_DIV = $(".error-message-container");

    public void logIn(String username, String password) {

        USERNAME_INPUT.sendKeys(username);
        PASSWORD_INPUT.sendKeys(password);
        LOGIN_BUTTON.click();

    }

    public HomePage logInSuccessfully(String username, String password) {

        logIn(username, password);
        return page(HomePage.class);

    }

    public void assertFailedLogIn(String errorMessage){

        ERROR_MESSAGE_DIV.shouldHave(Condition.exactText(errorMessage));

    }

    public void accessedLoginPage() {

        USERNAME_INPUT.shouldBe(Condition.visible);
        PASSWORD_INPUT.shouldBe(Condition.visible);
        LOGIN_BUTTON.shouldBe(Condition.visible);

    }
}