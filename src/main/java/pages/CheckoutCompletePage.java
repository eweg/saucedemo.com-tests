package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutCompletePage {

    private final SelenideElement HEADER_DIV = $("#header_container");
    private final SelenideElement PONY_EXPRESS_IMG = $(".pony_express");
    private final SelenideElement BACK_HOME_BUTTON = $("#back-to-products");


    public void assertAccessedCheckoutCompletePage() {

        HEADER_DIV.lastChild().shouldHave(Condition.exactText("Checkout: Complete!"));
        PONY_EXPRESS_IMG.shouldBe(Condition.visible);
        BACK_HOME_BUTTON.shouldBe(Condition.visible, Condition.enabled);

    }
}
